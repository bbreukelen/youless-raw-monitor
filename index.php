<?PHP
/////////////////////////////////////////////////////////////////////////
//
// Configuration

$youlessGas = "http://10.0.0.4"; // Path to your youless gas device. eg: http://10.0.0.4 or http://home.mydomain.nl:81
$youlessPwr = "http://10.0.0.5"; // Path to your youless power device. eg: http://10.0.0.5 or http://home.mydomain.nl:82

//
//
//
/////////////////////////////////////////////////////////////////////////
if ($_GET['act'] == 'pull') { getReading($_GET['device']); exit; }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>YouLess raw monitoring</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mootools/1.4.5/mootools-core-full-compat.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mootools-more/1.4.0.1/mootools-more-yui-compressed.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/3.0.10/adapters/mootools-adapter.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/3.0.10/highcharts.js"></script>
</head>
<body>
<div id="containerPwr" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br/><br/><br/><br/>
<div id="containerGas" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script>
var pwr = $('containerPwr');
var gas = $('containerGas');
pwr.theChart = new Highcharts.Chart({
	chart: { renderTo: pwr, zoomType: "x" },
	title: { text: 'RAW signal monitoring Youless Power', },
	xAxis: { title: { text: 'Measurements (500 ms)' } },
	yAxis: { title: { text: 'Signal' }, subtitle: { text: 'Measured per seconds' }, min: 0, max: 800 },
	plotOptions: { line: { marker: { enabled: false } } },
	legend: { enabled: false },
	series: [{ name: 'Power', data: [] }]
});
gas.theChart = new Highcharts.Chart({
	chart: { renderTo: gas, zoomType: "x" },
	title: { text: 'RAW signal monitoring Youless Gas', },
	xAxis: { title: { text: 'Seconds' } },
	yAxis: { title: { text: 'Signal' }, subtitle: { text: 'Measured per seconds' }, min: 0, max: 800 },
	plotOptions: { line: { marker: { enabled: false } } },
	legend: { enabled: false },
	series: [{ name: 'Gas', data: [] }]
});

function loadRaw() {
	var deviceType = this.deviceType;
	new Request({ 'url' : 'index.php?act=pull&device=' + deviceType, onSuccess: function(ret) {
		try {
			ret = JSON.decode(ret);
		} catch (err) {
			return;
		}
		if (ret.status == undefined || ret.status != 'success') { return; }
		
		var dev = (deviceType == 'pwr' ? pwr : gas);
		dev.theChart.series[0].addPoint(ret.data, true, false);
	}}).send();
}

loadRaw.periodical(1000, { 'deviceType' : 'pwr' });
loadRaw.periodical(1000, { 'deviceType' : 'gas' });
</script>
</body>
</html>
<?PHP
///////////////////////////////
function getReading($device) {
	global $youlessPwr, $youlessGas;
	$youless = ($device == 'pwr' ? $youlessPwr : $youlessGas);
	$youless = rtrim($youless, '/');
	$youlessUrl = $youless . "/a?f=j";
	$data = @file_get_contents($youlessUrl);
	$data = json_decode($data);
	if (isset($data->raw)) {
		print json_encode( Array( 'status' => 'success', 'data' => $data->raw ) );
	} else {
		print json_encode( Array( 'status' => 'error' ) );
	}
}
